package com.example.myapp

import android.icu.text.StringSearch
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp.databinding.FragmentHomeBinding
import java.util.*
import kotlin.collections.ArrayList


class
HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var newRecyclerView: RecyclerView
    private lateinit var newArrayList: ArrayList<News>
    private lateinit var tempArrayList: ArrayList<News>
    private lateinit var temp2ArrayList: ArrayList<News>
    lateinit var imageId: Array<Int>
    lateinit var title: Array<String>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        generateNews()

        newRecyclerView = binding.recyclerView
        newRecyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        newRecyclerView.setHasFixedSize(true)

        newArrayList = arrayListOf<News>()
        tempArrayList = arrayListOf<News>()
        temp2ArrayList = arrayListOf<News>()
        getUserData()

        val btnAdd = binding.btnAdd
        btnAdd.setOnClickListener {
            if (binding.textAdd.text.toString()!= "") {
                newArrayList.add(News(R.drawable.floare, binding.textAdd.text.toString()))
                tempArrayList.add(News(R.drawable.floare, binding.textAdd.text.toString()))
                Toast.makeText(
                    context,
                    "Stirea " + binding.textAdd.text.toString() + " a fost adaugata in lista",
                    Toast.LENGTH_SHORT
                ).show()
                newRecyclerView.adapter = MyAdapter(newArrayList)
                binding.textAdd.setText("")
            }
            else
                Toast.makeText(
                    context,
                    "Nu putem adauga o stire fara titlu!",
                    Toast.LENGTH_SHORT
                ).show()
        }



        val btnSearch = binding.btnSearch
        btnSearch.setOnClickListener {
            temp2ArrayList.clear()
            val textSearch = binding.textSearch.text.toString()
            //Toast.makeText(context, tempArrayList.size.toString(), Toast.LENGTH_SHORT).show()
            if (textSearch != "")
            {
//                for (j in tempArrayList.indices) {
//                    val news = News(imageId[j], title[j])
//                    if (news.newsTitle == textSearch)
//                    {
//                        temp2ArrayList.add(news)
//                    }

//                }
                if(tempArrayList.contains(News(R.drawable.floare, textSearch)))
                {
                    temp2ArrayList.add(News(R.drawable.floare, textSearch))
                }

                newRecyclerView.adapter = MyAdapter(temp2ArrayList)
            }

            else
            {
                newRecyclerView.adapter = MyAdapter(tempArrayList)
            }

        }

    }

    private fun getUserData() {
        for (i in imageId.indices) {
            val news = News(imageId[i], title[i])
            newArrayList.add(news)
            tempArrayList.add(news)
        }
        newRecyclerView.adapter = MyAdapter(newArrayList)
    }

    private fun generateNews() {
//        imageId = arrayOf(
//            R.drawable.a,
//            R.drawable.b,
//            R.drawable.c,
//            R.drawable.d,
//            R.drawable.e,
//            R.drawable.f,
//            R.drawable.g,
//            R.drawable.h,
//            R.drawable.i,
//            R.drawable.j,
//            R.drawable.k,
//            R.drawable.l,
//            R.drawable.m,
//            R.drawable.n,
//        )
        imageId = arrayOf(
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare,
            R.drawable.floare
        )
        title = arrayOf(
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",

            )
    }


}