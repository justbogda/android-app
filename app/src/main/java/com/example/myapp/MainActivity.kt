package com.example.myapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.myapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration:AppBarConfiguration
    private lateinit var mNavController:NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarViews.toolbarHome)
        setupNavDrawer()
    }

    private fun setupNavDrawer(){
        val navDrawer =binding.navView
        val drawerLayout = binding.navDrawerLayout
        val navHostFragment =supportFragmentManager.findFragmentById(R.id.fragmentHost) as NavHostFragment

        mNavController= navHostFragment.navController
        appBarConfiguration= AppBarConfiguration(
            setOf(
                R.id.homeFragment, R.id.favoriteFragment,R.id.profileFragment
            ), drawerLayout
        )
        setupActionBarWithNavController(mNavController, appBarConfiguration)
        navDrawer.setupWithNavController(mNavController)


    }

    override fun onSupportNavigateUp(): Boolean {
        return mNavController.navigateUp(appBarConfiguration)|| super.onSupportNavigateUp()
    }


}