package com.example.myapp

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapp.R
import com.example.myapp.databinding.FragmentFavoriteBinding


class FavoriteFragment : Fragment() {


    private var _binding:FragmentFavoriteBinding?=null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding= FragmentFavoriteBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnsharestatic.setOnClickListener {
            myShareStaticText()
            binding.textInput.setText("")
        }
        binding.btnstaticimg.setOnClickListener {
            myShareStaticImg()
        }
        binding.btnvartext.setOnClickListener {
            val textToSend =binding.textInput.text.toString()
            myShareVarText( textToSend)
        }

    }

    private fun myShareVarText(textToSend: String) {
        val intent =Intent().apply {
            action=Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT,textToSend)
            type="text/plain"
        }
        val shareIntent =Intent.createChooser(intent,null)
        startActivity(shareIntent)
    }

    fun myShareStaticText(){
        val message ="Salut \n\n"+
                "Am utilizat sharesheet intr-o aplicatie pentru a trimite acest mesaj"
        val intent =Intent().apply {
            action=Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT,message)
            type="text/plain"
        }
        val shareIntent =Intent.createChooser(intent,null)
        startActivity(shareIntent)

    }
    fun myShareStaticImg(){
        //var bitmap = (R.drawable.floare)
        val intent =Intent().apply {
            action=Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, Uri.EMPTY)
            type="image/jpeg"
        }
        val shareIntent =Intent.createChooser(intent,null)
        startActivity(shareIntent)

    }

}